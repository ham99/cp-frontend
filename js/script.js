var environment_test = false
var inst_count = 1;
var game_template = [
	[1, 'หมากฝรั่ง 1 ชิ้น', 'no'],
	[2, 'ขนมขบเคี้ยว 1 ถุง', 'no'],
	[3, 'น้ำเปล่า 600 มล. 1 ขวด', 'no'],
	[4, 'น้ำอัดลม 450 มล. 1 ขวด', 'no'],
	[5, 'ช็อกโกแลต 1 ชิ้น', 'no'],
	[6, 'ลูกอม 1 ถุง', 'no'],
	[7, 'ปากกา 1 ด้าม', 'no'],
	[8, 'เยลลี่ 1 ถุง', 'no'],
	[9, 'ซาลาเปา 2 ลูก', 'yes'],
	[10, 'ขนมจีบ 3 ลูก', 'yes'],
	// [11, 'ขนมปัง 1 ก้อน', 'no'],
	[12, 'ข้าวโพด 1 ฝัก', 'yes'],
	[13, 'นม 400 มล. 1 ขวด', 'no'],
	[14, 'ไอศกรีม 1 แท่ง', 'no'],
	[15, 'ข้าวกล่อง 1 กล่อง', 'yes'],
	[16, 'แชมพู 1 ขวด', 'no'],
	[17, 'สบู่ 1 ก้อน', 'no'],
	[18, 'แก้ว Gulp 1 แก้ว', 'no'],
	[19, 'ไส้กรอก 1 ชิ้น', 'yes'],
	[20, 'แปรงสีฟัน 1 ด้าม', 'no'],
	[21, 'ลูกอม+ช็อกโกแลต', 'no'],
	[22, 'น้ำอัดลมขวด + เยลลี่', 'no'],
	[23, 'ขนมปัง + นมกล่อง', 'no'],
	[24, 'ข้าวโพด + ขนมจีบ', 'yes'],
	[25, 'ช็อกโกแลต + ขนมขบเคี้ยว', 'no'],
	[26, 'แก้ว Gulp +ขนมขบเคี้ยว', 'no'],
	[27, 'แปรงสีฟัน + แชมพู', 'no'],
	[28, 'ข้าวกล่อง + ไส้กรอก', 'yes'],
	[29, 'ข้าวโพดฝัก + นมกล่อง', 'yes'],
	[30, 'สบู่ + แชมพู', 'no'],
	[31, 'น้ำอัดลม 1 กระป๋อง', 'no'],
	[32, 'นม 1 กล่อง', 'no'],
	[33, 'น้ำเปล่า 3 ขวด', 'yes'],
];

var game_load = game_template.slice(0);
var counting = game_load.length - 1;
var answer_status;
var score = 0;
var random_qoute = 6;
var audio = $("#bgm")[0];
var audio2 = $("#correct_sound")[0];
audio2.volume = 1;
var audio3 = $("#wrong_sound")[0];
audio3.volume = 1;
var audio4 = $("#end_sound")[0];

$(window).on('scroll touchmove mousewheel', function(e){
	  e.preventDefault();
	  e.stopPropagation();
	  return false;
	})

function isFacebookApp() {
    var ua = navigator.userAgent || navigator.vendor || window.opera;
    return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
}
function isiPhone() {

  var iDevices = [
    'iPhone Simulator',
    'iPod Simulator',
    'iPhone',
    'iPod'
  ];
  if (!!navigator.platform) {
    while (iDevices.length) {
      if (navigator.platform === iDevices.pop()){ return true; }
    }
  }
  return false;
}


$( document ).ready(function() {
	$(window).scrollTop(0);
	$('body').height(100);
	if(isFacebookApp() == true && isiPhone() == true){
		$('body').css('overflow','hidden');
		$('.view-container').css('height','calc(100% - 144px)')
	}
	else{
		
	}
	for (i = 1; i <= game_template.length; i++) { 
	    $( ".img_load" ).load( "images/product/"+i+".png" );
	}
	
	$(this).delay(400).queue(function() {
		$(window).scrollTop(0);
		$(this).delay(400).queue(function() {
			$(window).scrollTop(0);
			$(this).delay(400).queue(function() {
				$(window).scrollTop(0);
				$(this).delay(400).queue(function() {
					$(window).scrollTop(0);
					$(this).delay(2500).queue(function() {
						$(window).scrollTop(0);
						first_welcome();
				        $(this).dequeue();
				    });
			        $(this).dequeue();
			    });
		        $(this).dequeue();
		    });
	        $(this).dequeue();
	    });
	$(this).dequeue();
    });
    
});

function first_welcome(){
	$('.intro-main').css('display','block');
	$('.intro .intro-text').css('opacity','0');
	$('.intro .loading').css('opacity','0');
	$('.intro .next').css('display','block');
	$(this).delay(300).queue(function() {
		$('.logo').addClass('open');
		$(this).delay(800).queue(function() {
			$('.logo').addClass('swing');
			$('.intro .next').css('opacity','1');
	        $(this).dequeue();
	    });
        $(this).dequeue();
    });
}

function first_start(){
	

	$('.instruction').css('display','block');
	$(this).delay(400).queue(function() {
		$('.intro').css('opacity','0');
		$('.logo').addClass('inst');
		$(this).delay(100).queue(function() {
			$('.instruction').addClass('inst-show');
			$(this).delay(600).queue(function() {
		        $(this).dequeue();
		    });
	        $(this).dequeue();
	    });
        $(this).dequeue();
    });
}

function inst_next(){
	if(inst_count == 1)
	{
		if(environment_test)
			regis_start();
		$('.instruction .inst1').css('opacity','0');
		$('.instruction .inst2').css('opacity','1');
		inst_count++;
	}
	else if(inst_count == 2){
		$('.instruction .inst2').css('opacity','0');
		$('.instruction .inst3').css('opacity','1');
		inst_count++;
	}
	else if(inst_count == 3){
		inst_close();
	}
	
}

function inst_close(){
	$('.instruction').addClass('close');
	$('.seven').addClass('h_hide');
	$('.logo').addClass('h_hide');
	$('.top-shade').addClass('h_hide');
	$('.shade-loading').addClass('h_hide');
	$(this).delay(10).queue(function() {
		game_start();
		$(this).delay(800).queue(function() {
			$('.instruction').css('display','none');
			$('.instruction').removeClass('close');
			$('.instruction').removeClass('inst-show');
			$('.instruction .inst1').css('opacity','1');
			$('.instruction .inst2').css('opacity','0');
			$('.instruction .inst3').css('opacity','0');
			inst_count=1;
	        $(this).dequeue();
	    });
        $(this).dequeue();
    });
}

function game_start(){
	score=0;
	game_load = game_template.slice(0);
	counting = game_load.length - 1;
	audio.volume = 0;
	audio.play();
	audio.currentTime = 0;
	$('#bgm').animate({volume: 0.8}, 1050);
	// alert(game_load.length+'||'+game_template.length)
	$('.point-item-point span').html(score);
	$('span.timer').html('00.40');
	$('.game').css('display','block');
	$('.intro-bottom').removeClass('intro-show');
	$(this).delay(400).queue(function() {
		$('.game').addClass('game-show');
		game_random()
		$('.bottom').addClass('game-bottom');
		$(this).delay(200).queue(function() {
			$('.game .game-container .title').addClass('title-show');
			$(this).delay(450).queue(function() {
				// audio.volume = 0.4;
				$('.game .game-container .product-name').addClass('product-show');
				$('.game .game-container .product-img').addClass('product-show');
				$(this).delay(50).queue(function() {
					$('.game .point-container .point-item.point-item-time').addClass('point-show');
					
					$(this).delay(200).queue(function() {
						$('.game .point-container .point-item.point-item-point').addClass('point-show');
						$(this).delay(500).queue(function() {
							$('.top-shade').removeClass('h_hide');
							$('.shade-loading').removeClass('h_hide');
							$('.intro-main').css('display','none');
							countdown();
					        $(this).dequeue();
					    });
				        $(this).dequeue();
				    });
			        $(this).dequeue();
			    });
		        $(this).dequeue();
		    });
	        $(this).dequeue();
	    });
        $(this).dequeue();
    });
}

function countdown(){
	$('span.timer').html('00.40')
	var interval = setInterval(function() {
	    var timer = $('span.timer').html();
	    timer = timer.split('.');
	    var minutes = parseInt(timer[0], 10);
	    var seconds = parseInt(timer[1], 10);
	    seconds -= 1;
	    if (minutes < 0) return clearInterval(interval);
	    if (minutes < 10 && minutes.length != 2) minutes = '0' + minutes;
	    if (seconds < 0 && minutes != 0) {
	        minutes -= 1;
	        seconds = 59;
	    }
	    else if (seconds < 10 && length.seconds != 2) seconds = '0' + seconds;
	    $('span.timer').html(minutes + '.' + seconds);
	    if (minutes == 0 && seconds == 1)
	    {
	       	// $('.game-button-item').css('pointer-events','none');
	    }   
	    if (minutes == 0 && seconds == 0)
	    {
	        clearInterval(interval);
	        game_stop();
	    }   
	}, 1000);
}
function game_next(ans){
	
	if(ans == answer_status)
	{
		game_right();
	}
	else
	{
		game_wrong();
	}
};

function game_random(){
	if (counting >= 0)
	{
		console.log(game_template.length)
		var game_data = game_load.splice(Math.floor(Math.random() * (counting + 1)), 1)[0];
    	console.log(counting + ' || ' + game_data + ' || ' + game_load.length + ' || ' + game_template.length);
    	counting--;
    	$('.product-name').html(game_data[1]);
    	$('.product-img img').attr('src','images/product/'+game_data[0]+'.png');
    	answer_status = game_data[2];
	}
}

function game_right(){

	$('.game .game-container .product-name').removeClass('product-show');
	$('.game .game-container .product-img').removeClass('product-show');
	$(this).delay(10).queue(function() {
		audio2.currentTime = 0;
		audio2.play();
		$('.overlay-right').css('display','block');
		$(this).delay(180).queue(function() {
			$('.overlay-right').addClass('ans-show');
			$(this).delay(550).queue(function() {
				$('.product-img img').attr('src','');
				$('.overlay-right').removeClass('ans-show');
				score++;
				$('.point-item-point span').html(score);
				if (counting < 0)
				{
					game_stop();
				}
				else
				{
					game_random();
				}
				$(this).delay(200).queue(function() {
					
					$('.game .game-container .product-name').addClass('product-show');
					$('.game .game-container .product-img').addClass('product-show');
					$(this).delay(20).queue(function() {
						$('.overlay-right').css('display','none');
				        $(this).dequeue();
				    });
			        $(this).dequeue();
			    });
		        $(this).dequeue();
		    });
	        $(this).dequeue();
	    });
    	$(this).dequeue();
    });
}
function game_wrong(){
	
	$('.game .game-container .product-name').removeClass('product-show');
	$('.game .game-container .product-img').removeClass('product-show');
	$(this).delay(10).queue(function() {
		audio3.currentTime = 0;
		audio3.play();
		if(score > 0){
			score--;
			$('.point-item-point span').html(score);
		}
		$('.overlay-wrong').css('display','block');
		$(this).delay(180).queue(function() {
			$('.overlay-wrong').addClass('ans-show');
			$(this).delay(550).queue(function() {
				$('.product-img img').attr('src','');
				$('.overlay-wrong').removeClass('ans-show');
				if (counting < 0)
				{
					game_stop();
				}
				else
				{
					game_random();
				}
				$(this).delay(200).queue(function() {
					$('.game .game-container .product-name').addClass('product-show');
					$('.game .game-container .product-img').addClass('product-show');
					$(this).delay(20).queue(function() {
						$('.overlay-wrong').css('display','none');
				        $(this).dequeue();
				    });
			        $(this).dequeue();
			    });
		        $(this).dequeue();
		    });
	        $(this).dequeue();
	    });
	    $(this).dequeue();
	});
}

function game_stop(){
	$('.game').css('pointer-events','none');
	$(this).delay(300).queue(function() {
		$('.overlay-right').css('display','none !important');
		$('.overlay-wrong').css('display','none !important');
		$('#bgm').animate({volume: 0}, 900);
		$('.top-shade').addClass('h_hide');
		$('.shade-loading').addClass('h_hide');
		$(this).delay(200).queue(function() {
			audio4.currentTime = 0;
			audio4.play();
			$('.game .point-container .point-item.point-item-point').removeClass('point-show');
			$(this).delay(150).queue(function() {
				$('.game .point-container .point-item.point-item-time').removeClass('point-show');
				$(this).delay(150).queue(function() {
					$('.game .game-container .title').removeClass('title-show');
					$(this).delay(100).queue(function() {
						audio.pause();
						$('.game').removeClass('game-show');
						sum_start();
						$(this).delay(1000).queue(function() {
							$('.game').css('display','none');
							$('.game .game-container .product-name').removeClass('product-show');
							$('.game .game-container .product-img').removeClass('product-show');
							$('.game').css('pointer-events','inherit');
						    $(this).dequeue();
						    
						});
					    $(this).dequeue();
					});
				    $(this).dequeue();
				});
			    $(this).dequeue();
			});
		    $(this).dequeue();
		});
	    $(this).dequeue();
	});
}


function sum_start(){
	$('.bottom').removeClass('game-bottom');
	$('.sum').css('display','block');
	$('.sum .point-count .number').html(score);
	var pic = Math.floor(Math.random() * random_qoute) + 1;
	$('.sum .qoute').attr('src','images/qoute/'+pic+'.png')
	$(this).delay(200).queue(function() {
		$('.sum').addClass('sum-show');
		$(this).delay(10).queue(function() {
			$('.sum-tree').addClass('tree-show');
			$('.top-shade').removeClass('h_hide');
			$('.shade-loading').removeClass('h_hide');
	        $(this).dequeue();
	    });
        $(this).dequeue();
    });
}

function sum_back(){
	$('.sum').css('pointer-events','none');
	$('.sum-tree').removeClass('tree-show');
	$(this).delay(300).queue(function() {
		$('.sum').removeClass('sum-show');
		$(this).delay(100).queue(function() { 
			$('.top-shade').addClass('h_hide');
			$('.shade-loading').addClass('h_hide');
			$(this).delay(600).queue(function() {
			$('.sum').css('display','none');
				$('.sum').css('pointer-events','inherit');
				welcome_again();
		        $(this).dequeue();
		    });
	        $(this).dequeue();
	    });
	    $(this).dequeue();
	});
}

function sum_address(){
	$('.sum-tree').removeClass('tree-show');
	$(this).delay(300).queue(function() {
		$('.sum').removeClass('sum-show');
		$(this).delay(100).queue(function() { 
			$('.top-shade').addClass('h_hide');
			$('.shade-loading').addClass('h_hide');
			$(this).delay(600).queue(function() {
			$('.sum').css('display','none');
				regis_start();
		        $(this).dequeue();
		    });
	        $(this).dequeue();
	    });
	    $(this).dequeue();
	});
}

function regis_start(){
	$('.regis-button-item.sent').css('display','inline-block');
	$('.regis').css('display','block');
	$('.regis-desc').val('');
	$(this).delay(20).queue(function() {
		$('.regis').addClass('regis-show');
		$('.regis-bag').addClass('regis-show');
		$(this).delay(200).queue(function() {
			$('.top-shade').removeClass('h_hide');
			$('.shade-loading').removeClass('h_hide');
	        $(this).dequeue();
	    });
        $(this).dequeue();
    });
}
function regis_back(){
	$('.regis').css('pointer-events','none');
	$('.regis').removeClass('regis-show');
	$('.regis-bag').removeClass('regis-show');
	$('.top-shade').addClass('h_hide');
	$('.shade-loading').addClass('h_hide');
	$(this).delay(20).queue(function() {
		$(this).delay(600).queue(function() {
			$('.regis').css('display','none');
			$('.regis').css('pointer-events','inherit');
			welcome_again();
	        $(this).dequeue();
	    });
        $(this).dequeue();
    });
}

function regis_popup_open(){
	$('.regis-popup .finish').css('display','none');
	$('.regis-popup .loading').addClass('popup-show');
	$('.regis-popup').css('display','block');
	$(this).delay(600).queue(function() {
		$('.regis-popup').addClass('popup-open');
	    $(this).dequeue();
	});
}

function regis_popup_finish(){
	$('.regis-popup .finish').css('display','block');
	$('.regis-popup .loading').removeClass('popup-show');
	$(this).delay(600).queue(function() {
		$('.regis-popup .finish').addClass('popup-show');
	    $(this).dequeue();
	});
}

function regis_popup_close(){
	$('.regis-popup .finish').removeClass('popup-show');
	$('.regis-button-item.sent').css('display','none');
	$(this).delay(600).queue(function() {
		$('.regis-popup .finish').css('display','none');
		$('.regis-popup').removeClass('popup-open');
		$(this).delay(600).queue(function() {
			$('.regis-popup').css('display','none');
			// regis_back();
		    $(this).dequeue();
		});
	    $(this).dequeue();
	});
}

function regis_popup_close_w_sent_button(){
	$('.regis-popup .finish').removeClass('popup-show');
	$(this).delay(600).queue(function() {
		$('.regis-popup .finish').css('display','none');
		$('.regis-popup').removeClass('popup-open');
		$(this).delay(600).queue(function() {
			$('.regis-popup').css('display','none');
			// regis_back();
		    $(this).dequeue();
		});
	    $(this).dequeue();
	});
}

function AMAMWASOFOFOF(naruto, sikamaru, sazuke, mama, FUNC, FUNC_ERROR) {
	const client = new Lokka({
		transport: new LokkaTransport('https://api.graph.cool/simple/v1/cj4rbixn7n3yp0185stiwoen4')
	});
	sikamaru = encodeURIComponent(sikamaru);
	mama = encodeURIComponent(mama);
	var ASD = '' + naruto + ':' + sikamaru + ':' + sazuke;
	var KASD = CryptoJS.HmacSHA512(ASD, "naruto").toString()
	var sss = parseInt(naruto);
	var command = "($score: Int!, $name:String!, $telephone:String!, $description:String, $rawScore: String!){createScoreRecord(score: $scorename: $nametelephone: $telephonerawScore: $rawScoredescription: $description) {id}}";
	client.mutate(command, { score: sss, name: sikamaru, telephone: sazuke, rawScore: KASD, description: mama }).then(function(){on_regis_finish()}).catch(function(err){ alert("การเชื่อมต่อขัดข้อง กรุณาลองใหม่อีกครั้ง");});
}

function on_regis(){
	var name = $('#name').val();
	var tel = $('#tel').val();
	var desc = $('#desc').val();

	if (!name || name.length < 5){
		alert('กรุณาใส่ชื่อ-นามสกุล มากกว่า 5 ตัวอักษร')
		return;
	}
	if (!tel || tel.length < 9){
		alert('กรุณาเบอร์โทร 9-10 หลัก')
		return;
	}
	if (!name || desc.length < 10){
		alert('กรุณาใส่วิธีไม่รับถุง มากกว่า 10 ตัวอักษร')
		return;
	}
	AMAMWASOFOFOF(score, name, tel, desc)
	regis_popup_open();
}

function on_regis_finish(){
	regis_popup_finish();
	// alert('ส่งคะแนนสำเร็จ')
	// setTimeout( () => {
	// 	regis_back();
	// }, 2000)
}

function share_facebook(){
	var url_share = document.getElementById("share-url").textContent;
	var base_url = 'https://www.facebook.com/sharer/sharer.php?u=';
	var url = url_share + score.toString();
	//document.getElementsByClassName('fb-share-button').item(0).firstElementChild.parentElement.setAttribute('data-href', url);
	//document.getElementsByClassName('fb-share-button').item(0).firstElementChild.parentElement.click();
	document.getElementById('fb-share-version2').setAttribute('href', base_url + url);
	document.getElementById('fb-share-version2').click();
}

function welcome_again(){
	$(this).delay(10).queue(function() {
		$('.intro-main').css('display','block');
		$('.intro-bottom').addClass('intro-show');
		$('.seven').removeClass('h_hide');
		$('.logo').removeClass('swing');
		$('.logo').removeClass('open');
		$('.logo').removeClass('inst');
		$('.logo').removeClass('h_hide');
		$('.logo').addClass('open');
		$(this).delay(500).queue(function() {
			$('.logo').addClass('swing');
			$('.top-shade').removeClass('h_hide');
			$('.shade-loading').removeClass('h_hide');
			$('.bottom').removeClass('game-bottom');
			$(this).delay(100).queue(function() {
				$('.intro').css('opacity','1');
		        $(this).dequeue();
		    });
	        $(this).dequeue();
	    });
        $(this).dequeue();
    });
}

$('#tel').on('keypress', function(ev) {
    var keyCode = window.event ? ev.keyCode : ev.which;
    //codes for 0-9
    if (keyCode < 48 || keyCode > 57) {
        //codes for backspace, delete, enter
        if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey && keyCode != 43 && keyCode != 45) {
            ev.preventDefault();
        }
    }
});



